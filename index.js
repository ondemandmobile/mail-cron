var express = require("express");
var cors = require("cors");
const axios = require("axios");
var moment = require("moment");
const xokoLogo = "https://app.xoko.co.ke/images/xoko_logo_flat.jpg";
const mailsenderEndpoint = "https://app.xoko.co.ke/Reports";
var reportTypes = [
  "General Sales",
  "Staff Sales",
  "Branch Sales",
  "Payments",
  "Cashfloats",
  "Branch Cashfloat",
];

var reportTypesTest = ["Staff Sales", "Branch Sales", "Payments", "Cashfloats"];
axios.defaults.headers.post["Content-Type"] = "application/json";
axios.defaults.timeout = 2500;
const app = express();
const cron = require("node-cron");
var jwt = require("jsonwebtoken");
let hostname = process.env.HOSTNAME || "localhost";
let port = process.env.PORT || 3006;
let myenvironment = process.env.NODE_ENV || "development";
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
// cron every 2 minutes '*/2 * * * *'
if (myenvironment !== "production") {
  cron.schedule("* * * * *", function () {
    for (var i = 0; i < reportTypes.length; i++) {
      var reportType = reportTypes[i];
      console.log(
        "Testing to see subscribers for reportType " + reportType + " exist"
      );
      getSubscribers(reportTypes[i], 1, function (subsData) {
        if (subsData) {
          console.log(subsData);
        }
      });
    }
  });
}
cron.schedule("50 22 * * *", function () {
  console.log("sending daily reports");
  if (myenvironment === "production") {
    for (var i = 0; i < reportTypes.length; i++) {
      getSubscribers(reportTypes[i], 1, function (subsData) {
        if (subsData) {
          for (var j = 0; j < subsData.length; j++) {
            (function (j) {
              setTimeout(function () {
                if (subsData[j].subscribers.length > 0) {
                  subsData[j].subscribers = subsData[j].subscribers.filter(
                    function (str) {
                      return /\S/.test(str);
                    }
                  );
                  subsData[j].subscribers.forEach((element, index) => {
                    subsData[j].subscribers[index] = element.trim();
                  });
                }
                toMailSender(mailsenderEndpoint, {
                  storeid: subsData[j].storeid, //from the stores loop
                  reportType: subsData[j].report_type,
                  fromdate: moment().format("YYYY-MM-DD"),
                  todate: moment().format("YYYY-MM-DD"),
                  emailSpecifics: {
                    email_title:
                      "Today's " +
                      subsData[j].report_type +
                      " Summary Report - " +
                      moment().format("MMMM") +
                      " " +
                      moment().format("DD") +
                      ", " +
                      moment().format("YYYY"),
                    subscribers: subsData[j].subscribers,
                    logo: subsData[j].logo ? subsData[j].logo : xokoLogo,
                    period: 1,
                  },
                  hideWarnings: true,
                });
              }, 120000 * j); //2 minutes gap
            })(j);
          }
        }
      });
    }
  }
});
//cron for sending weekly reports 11:30 on sundays
cron.schedule("50 22 * * *", function () {
  console.log("sending weekly reports");
  if (myenvironment === "production") {
    if (
      moment().format("YYYY-MM-DD") ===
      moment().endOf("week").format("YYYY-MM-DD")
    ) {
      for (var i = 0; i < reportTypes.length; i++) {
        getSubscribers(reportTypes[i], 2, function (subsData) {
          if (subsData) {
            for (var j = 0; j < subsData.length; j++) {
              (function (j) {
                setTimeout(function () {
                  if (subsData[j].subscribers.length > 0) {
                    subsData[j].subscribers = subsData[j].subscribers.filter(
                      function (str) {
                        return /\S/.test(str);
                      }
                    );
                    subsData[j].subscribers.forEach((element, index) => {
                      subsData[j].subscribers[index] = element.trim();
                    });
                  }
                  toMailSender(mailsenderEndpoint, {
                    storeid: subsData[j].storeid, //from the stores loop
                    reportType: subsData[j].report_type,
                    fromdate: moment().startOf("week").format("YYYY-MM-DD"),
                    todate: moment().endOf("week").format("YYYY-MM-DD"),
                    emailSpecifics: {
                      email_title:
                        "Your Weekly " +
                        subsData[j].report_type +
                        " Summary Report for " +
                        moment().startOf("week").format("MMMM") +
                        " " +
                        moment().startOf("week").format("DD") +
                        ", " +
                        moment().startOf("week").format("YYYY") +
                        "  - " +
                        moment().endOf("week").format("MMMM") +
                        " " +
                        moment().endOf("week").format("DD") +
                        "," +
                        moment().startOf("week").format("YYYY"),
                      subscribers: subsData[j].subscribers,
                      logo: subsData[j].logo ? subsData[j].logo : xokoLogo,
                      period: 2,
                    },
                    hideWarnings: true,
                  });
                }, 120000 * j);
              })(j);
            }
          }
        });
      }
    }
  }
});
//monthly cron
cron.schedule("50 22 * * *", function () {
  console.log("sending monthly reports");
  if (myenvironment === "production") {
    if (
      moment().format("YYYY-MM-DD") ===
      moment().endOf("month").format("YYYY-MM-DD")
    ) {
      for (var i = 0; i < reportTypes.length; i++) {
        getSubscribers(reportTypes[i], 3, function (subsData) {
          if (subsData) {
            for (var j = 0; j < subsData.length; j++) {
              (function (j) {
                setTimeout(function () {
                  if (subsData[j].subscribers.length > 0) {
                    subsData[j].subscribers = subsData[j].subscribers.filter(
                      function (str) {
                        return /\S/.test(str);
                      }
                    );
                    subsData[j].subscribers.forEach((element, index) => {
                      subsData[j].subscribers[index] = element.trim();
                    });
                  }
                  toMailSender(mailsenderEndpoint, {
                    storeid: subsData[j].storeid, //from the stores loop
                    reportType: subsData[j].report_type,
                    fromdate: moment().startOf("month").format("YYYY-MM-DD"),
                    todate: moment().endOf("month").format("YYYY-MM-DD"),
                    emailSpecifics: {
                      email_title:
                        moment().format("MMMM") +
                        ", " +
                        moment().format("YYYY") +
                        ", " +
                        subsData[j].report_type +
                        " Summary",
                      subscribers: subsData[j].subscribers,
                      logo: subsData[j].logo ? subsData[j].logo : xokoLogo,
                      period: 3,
                    },
                    hideWarnings: true,
                  });
                }, 120000 * j);
              })(i);
            }
          }
        });
      }
    }
  }
});
//yearly cron
cron.schedule("50 22 * * *", function () {
  console.log("sending yearly reports");
  if (myenvironment === "production") {
    if (
      moment().format("YYYY-MM-DD") ===
      moment().endOf("year").format("YYYY-MM-DD")
    ) {
      for (var i = 0; i < reportTypes.length; i++) {
        getSubscribers(reportTypes[i], 4, function (subsData) {
          if (subsData) {
            for (var j = 0; j < subsData.length; j++) {
              (function (j) {
                setTimeout(function () {
                  if (subsData[j].subscribers.length > 0) {
                    subsData[j].subscribers = subsData[j].subscribers.filter(
                      function (str) {
                        return /\S/.test(str);
                      }
                    );
                    subsData[j].subscribers.forEach((element, index) => {
                      subsData[j].subscribers[index] = element.trim();
                    });
                  }
                  toMailSender(mailsenderEndpoint, {
                    storeid: subsData[j].storeid, //from the stores loop
                    reportType: subsData[j].report_type,
                    fromdate: moment().startOf("year").format("YYYY-MM-DD"),
                    todate: moment().endOf("year").format("YYYY-MM-DD"),
                    emailSpecifics: {
                      email_title:
                        moment().format("YYYY") +
                        ", Yearly " +
                        subsData[j].report_type +
                        " Summary",
                      subscribers: subsData[j].subscribers,
                      logo: subsData[j].logo ? subsData[j].logo : xokoLogo,
                      period: 4,
                    },
                    hideWarnings: true,
                  });
                }, 120000 * j);
              })(i);
            }
          }
        });
      }
    }
  }
});
function toMailSender(endpoint, payload) {
  console.log("payload to mail sender", payload);
  axios
    .post(endpoint, payload)
    .then(function (response) {
      console.log("sent to mail sender", payload);
      console.log(response);
    })
    .catch(function (error) {
      console.log(error);
    });
}
function getSubscribers(reportType, period, callback) {
  getToken(function (token) {
    if (token) {
      var payload = {
        payload: { report_type: reportType, period: period },
        token: token,
      };
      axios
        .post("https://app.xoko.co.ke/api/store/findAllReportSubs", payload)
        .then(function (response) {
          return callback(response.data);
        })
        .catch(function (error) {
          console.log(error);
          return callback("");
        });
    } else console.log("Sorry there is no Token for fetching subscribers");
  });
}
function getToken(callback) {
  var expiry = Math.floor(Date.now() / 1000) + 720 * 60 * 2 * 2; //2-days of session
  var nbf = Math.floor(Date.now() / 1000) - 30;
  var consumer_key = "Bcmv4ob6IjzvFtz0prSp7GhNDLgNOryv";
  var consumer_secret = "K8HCfk39C2y4K3XopOUsC7CNaN4ZFcge";
  var token = jwt.sign(
    {
      exp: expiry,
      nbf: nbf,
      iss: consumer_key,
      data: {
        storeid: 225,
        person_id: 5,
      },
    },
    consumer_secret
  );
  return callback(token);
}
try {
  app.listen(port, () =>
    console.log(
      `Http server listening at http://${hostname}:${port}/ in ${myenvironment} mode`
    )
  );
} catch (error) {
  console.log("app failed to launch" + error);
}
