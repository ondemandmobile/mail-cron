module.exports = {
  apps: [
    {
      name: "mail_cron_service",
      script: "index.js",
      env_staging: {
        PORT: 3006,
        NODE_ENV: "staging",
      },
      env_production: {
        PORT: 3006,
        NODE_ENV: "production",
      },
    },
  ],
  deploy: {
    staging: {
      user: "ubuntu",
      host: "ec2-13-245-176-133.af-south-1.compute.amazonaws.com",
      key: "~/.ssh/sa-key.pem",
      ref: "origin/main",
      repo: "git@bitbucket.org:ondemandmobile/mail-cron.git",
      path: "/home/ubuntu/apis/mail-cron",
      "pre-setup": "echo 'Setting up repo on STAGING server.'",
      "pre-deploy-local": "echo 'Deploying code to STAGING server'",
      "post-deploy":
        "npm install && pm2 startOrRestart ecosystem.config.js --env staging && pm2 save",
    },
    production: {
      user: "ubuntu",
      host: "ec2-52-34-34-66.us-west-2.compute.amazonaws.com",
      key: "~/.ssh/axlion.pem",
      ref: "origin/master",
      repo: "git@bitbucket.org:ondemandmobile/mail-cron.git",
      path: "/home/ubuntu/apis/mail-cron",
      "pre-setup": "echo 'Setting up repo on PRODUCTION server.'",
      "pre-deploy-local": "echo 'Deploying code to PRODUCTION server'",
      "post-deploy":
        "npm install && pm2 startOrRestart ecosystem.config.js --env production && pm2 save",
    },
  },
};
